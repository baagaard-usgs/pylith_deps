export BUILDBOT=linux_x86_64
export PYTHON_VERSION=2.7

PYLITHDEPS_DIR=${HOME}/pylith/dependencies
PATH=${PYLITHDEPS_DIR}/bin:${PATH}
export LD_LIBRARY_PATH=${PYLITHDEPS_DIR}/lib
export PYTHONPATH=${PYLITHDEPS_DIR}/lib/python${PYTHON_VERSION}/site-packages
unset PYLITHDEPS_DIR

BUILDBOT_INSTALL=${HOME}/pylith/install/${BUILDBOT}
PATH=${BUILDBOT_INSTALL}/bin:${PATH}
export LD_LIBRARY_PATH=${BUILDBOT_INSTALL}/lib:${BUILDBOT_INSTALL}/lib64:${LD_LIBRARY_PATH}
export PYTHONPATH=${BUILDBOT_INSTALL}/lib/python${PYTHON_VERSION}/site-packages:${PYTHONPATH}
unset BUILDBOT_INSTALL

if [ "${BUILDBOT}" == "darwin_10.5" ] || [ ${BUILDBOT} == "darwin_10.6" ]; then
  unset LD_LIBRARY_PATH
fi

if [ "${BUILDBOT}" == "cygwin_i686" ]; then
  export PATH=${PATH}:${HOME}/pylith/install/${BUILDBOT}/lib
  export PATH=${PATH}:/lib/lapack
  unset LD_LIBRARY_PATH
fi
