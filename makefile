ifeq ($(BUILDBOT), linux_x86_64)
  FETCH=wget -nd -r
  mpich_sharedoptions=--enable-shared --enable-sharedlibs=gcc --disable-static
  MAKE_THREADS=2
endif

ifeq ($(BUILDBOT), linux_i686)
  FETCH=wget -nd -r
  mpich_sharedoptions=--enable-shared --enable-sharedlibs=gcc --disable-static
  MAKE_THREADS=2
endif

ifeq ($(BUILDBOT), darwin_10.5)
  FETCH=curl -O
  mpich_sharedoptions=--enable-shared --enable-sharedlibs=osx-gcc --disable-static
  env_flags=CFLAGS="-mmacosx-version-min=10.5 -isysroot $(SDKROOT)" CXXFLAGS="-mmacosx-version-min=10.5  -isysroot $(SDKROOT)" FFLAGS="-mmacosx-version-min=10.5  -isysroot $(SDKROOT)"
  GCC_EXTRAARGS=--with-dwarf2
  MAKE_THREADS=2
endif

ifeq ($(BUILDBOT), darwin_10.6)
  FETCH=curl -O
  mpich_sharedoptions=--enable-shared --enable-sharedlibs=osx-gcc --disable-static
  env_flags=CFLAGS="-mmacosx-version-min=10.6 -isysroot $(SDKROOT)" CXXFLAGS="-mmacosx-version-min=10.6  -isysroot $(SDKROOT)" FFLAGS="-mmacosx-version-min=10.6  -isysroot $(SDKROOT)"
  GCC_EXTRAARGS=--with-dwarf2
  MAKE_THREADS=2
endif

ifeq ($(BUILDBOT), cygwin)
  FETCH=wget -nd -r
  MAKE_THREADS=1
  mpich_sharedoptions=--enable-shared --enable-sharedlibs=libtool --disable-static
endif

# ----------------------------------------------------------------------
# Package Versions
# ----------------------------------------------------------------------
INSTALL_DIR=$(HOME)/pylith/install/$(BUILDBOT)
DEPS_DIR=$(HOME)/pylith/dependencies

GCC_VER=4.8.2
MPC_VER=1.0.2
GMP_VER=5.1.3
MPFR_VER=3.1.2

PYTHON_VER=2.7.6

MPICH_VER=3.0.4

CPPUNIT_VER=1.12.1-patched

PCRE_VER=8.12
SWIG_VER=2.0.6

GIT_VER=1.8.2.1

NUMPY_VER=1.7.0

PROJ4_VER=4.8.0

HDF5_VER=1.8.8
H5PY_VER=2.0.1

NETCDF_VER=4.1.3
NETCDFPY_VER=1.0

SCIENTIFICPYTHON_VER=2.9.1
FIAT_VER=0.9.9


default:

linux: gcc python mpich cppunit pcre swig numpy fiat proj hdf5 h5py netcdf netcdfpy pythia nemesis

darwin: gcc python mpich cppunit pcre swig numpy fiat proj hdf5 h5py netcdf netcdfpy pythia nemesis

cygwin: mpich numpy fiat proj hdf5 h5py netcdf netcdfpy pythia nemesis

gnubuild:
	$(FETCH) http://geodynamics.org/~buildbot/deps/m4-1.4.16.tar.gz
	$(FETCH) http://geodynamics.org/~buildbot/deps/autoconf-2.68.tar.gz
	$(FETCH) http://geodynamics.org/~buildbot/deps/automake-1.11.1.tar.gz
	$(FETCH) http://geodynamics.org/~buildbot/deps/libtool-2.4.tar.gz
	tar --no-same-owner -zxf m4-1.4.16.tar.gz
	mkdir -p m4-1.4.16-build
	cd m4-1.4.16-build && \
		../m4-1.4.16/configure --prefix=$(DEPS_DIR) $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install
	tar --no-same-owner -zxf autoconf-2.68.tar.gz
	mkdir -p autoconf-2.68-build
	cd autoconf-2.68-build && \
		../autoconf-2.68/configure --prefix=$(DEPS_DIR) $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install
	tar --no-same-owner -zxf automake-1.11.1.tar.gz
	mkdir -p automake-1.11.1-build
	cd automake-1.11.1-build && \
		../automake-1.11.1/configure --prefix=$(DEPS_DIR) $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install
	tar --no-same-owner -zxf libtool-2.4.tar.gz
	mkdir -p libtool-2.4-build
	cd libtool-2.4-build && \
		../libtool-2.4/configure --prefix=$(DEPS_DIR)  $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install


gcc:
	$(FETCH) http://geodynamics.org/~buildbot/deps/gcc-$(GCC_VER).tar.bz2
	$(FETCH) http://geodynamics.org/~buildbot/deps/mpc-$(MPC_VER).tar.gz
	$(FETCH) http://geodynamics.org/~buildbot/deps/gmp-$(GMP_VER).tar.gz
	$(FETCH) http://geodynamics.org/~buildbot/deps/mpfr-$(MPFR_VER).tar.bz2
	tar --no-same-owner -jxf gcc-$(GCC_VER).tar.bz2
	cd gcc-$(GCC_VER); \
		tar --no-same-owner -zxf ../gmp-$(GMP_VER).tar.gz; \
		ln -sf gmp-$(GMP_VER)/ gmp; \
		tar --no-same-owner -zxf ../mpc-$(MPC_VER).tar.gz; \
		ln -sf mpc-$(MPC_VER)/ mpc; \
		tar --no-same-owner -jxf ../mpfr-$(MPFR_VER).tar.bz2; \
		ln -sf mpfr-$(MPFR_VER)/ mpfr
	mkdir -p gcc-$(GCC_VER)-build
	cd gcc-$(GCC_VER)-build && \
		../gcc-$(GCC_VER)/configure --prefix=$(INSTALL_DIR) \
			--enable-languages=c,c++,fortran \
			--disable-multilib \
			$(GCC_EXTRAARGS)  $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install


python:
	$(FETCH) http://geodynamics.org/~buildbot/deps/Python-$(PYTHON_VER).tgz
	tar --no-same-owner -zxf Python-$(PYTHON_VER).tgz
	mkdir -p Python-$(PYTHON_VER)-build
	cd Python-$(PYTHON_VER)-build && \
		../Python-$(PYTHON_VER)/configure --enable-shared --prefix=$(INSTALL_DIR) $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install


mpich:
	$(FETCH) http://geodynamics.org/~buildbot/deps/mpich-$(MPICH_VER).tar.gz
	tar --no-same-owner -zxf mpich-$(MPICH_VER).tar.gz
	mkdir -p mpich-$(MPICH_VER)-build
	cd mpich-$(MPICH_VER)-build && \
		../mpich-$(MPICH_VER)/configure --with-pm=gforker \
			$(mpich_sharedoptions) \
			--disable-mpe \
			--prefix=$(INSTALL_DIR) \
			CC=gcc CXX=g++ FC=gfortran $(env_flags) && \
		make && \
		make install
	if [ ! -z $(INSTALL_DIR)/bin/mpirun ]; then cd $(INSTALL_DIR)/bin && ln -s mpiexec mpirun; fi


cppunit:
	$(FETCH) http://geodynamics.org/~buildbot/deps/cppunit-1.12.1.tar.gz
	tar --no-same-owner -zxf cppunit-1.12.1.tar.gz
	mkdir -p cppunit-1.12.1-build
	cd cppunit-1.12.1-build && \
		../cppunit-1.12.1/configure --prefix=$(DEPS_DIR) $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install


pcre:
	$(FETCH) http://geodynamics.org/~buildbot/deps/pcre-$(PCRE_VER).tar.gz
	tar --no-same-owner -zxf pcre-$(PCRE_VER).tar.gz
	mkdir -p pcre-$(PCRE_VER)-build
	cd pcre-$(PCRE_VER)-build && \
		../pcre-$(PCRE_VER)/configure --prefix=$(DEPS_DIR) $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install


swig:
	$(FETCH) http://geodynamics.org/~buildbot/deps/swig-$(SWIG_VER).tar.gz
	tar --no-same-owner -zxf swig-$(SWIG_VER).tar.gz
	mkdir -p swig-$(SWIG_VER)-build
	cd swig-$(SWIG_VER)-build && \
		../swig-$(SWIG_VER)/configure --prefix=$(DEPS_DIR) $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install


numpy:
	$(FETCH) http://geodynamics.org/~buildbot/deps/numpy-$(NUMPY_VER).tar.gz
	tar --no-same-owner -zxf numpy-$(NUMPY_VER).tar.gz
	cd numpy-$(NUMPY_VER) && python setup.py install --prefix=$(INSTALL_DIR)

fiat:
	$(FETCH) http://geodynamics.org/~buildbot/deps/ScientificPython-2.9.1.tar.gz
	tar --no-same-owner -zxf ScientificPython-2.9.1.tar.gz
	cd ScientificPython-2.9.1 && python setup.py install --prefix=$(INSTALL_DIR)
	$(FETCH) http://geodynamics.org/~buildbot/deps/fiat-0.9.9.tar.gz
	tar --no-same-owner -zxf fiat-0.9.9.tar.gz
	cd fiat-0.9.9 && python setup.py install --prefix=$(INSTALL_DIR)

proj:
	$(FETCH) http://geodynamics.org/~buildbot/deps/proj-$(PROJ4_VER).tar.gz
	$(FETCH) http://geodynamics.org/~buildbot/deps/proj-datumgrid-1.3.zip
	tar --no-same-owner -zxf proj-$(PROJ4_VER).tar.gz
	mkdir -p proj-$(PROJ4_VER)-build/nad
	cd proj-$(PROJ4_VER)-build/nad && \
		unzip ../../proj-datumgrid-1.3.zip
	cd proj-$(PROJ4_VER)-build && \
		../proj-$(PROJ4_VER)/configure --prefix=$(INSTALL_DIR) $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install

hdf5:
	$(FETCH) http://geodynamics.org/~buildbot/deps/hdf5-$(HDF5_VER).tar.gz
	tar --no-same-owner -zxvf hdf5-$(HDF5_VER).tar.gz
	mkdir -p hdf5-$(HDF5_VER)-build
	cd hdf5-$(HDF5_VER)-build && \
		../hdf5-$(HDF5_VER)/configure --enable-fortran --enable-parallel --enable-shared --disable-static --enable-unsupported --prefix=$(INSTALL_DIR) CC=mpicc CXX=mpicxx FC=mpif90  $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install

h5py:
	$(FETCH) http://geodynamics.org/~buildbot/deps/h5py-$(H5PY_VER).tar.gz
	tar --no-same-owner -zxvf h5py-$(H5PY_VER).tar.gz
	cd h5py-$(H5PY_VER) && \
		python setup.py build --hdf5=$(INSTALL_DIR) && \
		python setup.py install --prefix=$(INSTALL_DIR)

netcdf:
	$(FETCH) http://geodynamics.org/~buildbot/deps/netcdf-$(NETCDF_VER).tar.gz
	tar --no-same-owner -zxvf netcdf-$(NETCDF_VER).tar.gz
	mkdir -p netcdf-$(NETCDF_VER)-build
	cd netcdf-$(NETCDF_VER)-build && \
		../netcdf-$(NETCDF_VER)/configure --prefix=$(INSTALL_DIR) \
			--enable-shared --disable-static --enable-netcdf-4 --disable-dap  $(env_flags) && \
		make -j $(MAKE_THREADS) && \
		make install

netcdfpy:
	$(FETCH) http://geodynamics.org/~buildbot/deps/netCDF4-1.0fix1.tar.gz
	tar --no-same-owner -zxvf netCDF4-1.0fix1.tar.gz
	cd netCDF4-$(NETCDFPY_VER) && \
		echo "[options]\nuse_ncconfig=True" > setup.cfg && \
		python setup.py build && \
		python setup.py install --prefix=$(INSTALL_DIR)

pythia:
	git clone --depth 1 https://github.com/geodynamics/pythia.git  pythia-dev
	cd pythia-dev && python setup.py install --prefix=$(INSTALL_DIR)

nemesis:
	git clone --depth 1 --recursive https://github.com/geodynamics/nemesis.git nemesis-dev
	cd nemesis-dev && autoreconf -if
	mkdir -p nemesis-build
	cd nemesis-build && \
		../nemesis-dev/configure --prefix=$(INSTALL_DIR) $(env_flags) && \
		make && \
		make install

cleanup_x86_64:
	rm -fr $(INSTALL_DIR)/lib32
